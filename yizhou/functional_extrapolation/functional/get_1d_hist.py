import ROOT as root

real_rate = root.TFile.Open("../hist_real_1btag_4jets.root")
for histname in ["real_lepvstopo_highDR_re","real_lepvstopo_lowDR_re","real_lepvstopo_highDR_rmu","real_lepvstopo_medDR_rmu","real_lepvstopo_lowDR_rmu"]:
    hist = real_rate.Get(histname)
    hist_ntup = root.TFile(histname+".root", "RECREATE")
    hist_ntup.cd()
    hist_ybins = hist.ProjectionY("bins",1,1)
    for i in range(hist.GetNbinsY()):
        ii = i+1
        hist_xbin = hist.ProjectionX(str(hist_ybins.GetBinLowEdge(ii))+"_"+str(hist_ybins.GetBinUpEdge(ii)),ii,ii)
        hist_xbin.Write()
    hist_ntup.Close()

fake_rate = root.TFile.Open("../hist_fake_1btag_4jets.root")
for histname in ["fake_lepvstopo_highDR_re","fake_lepvstopo_lowDR_re","fake_lepvstopo_highDR_rmu","fake_lepvstopo_medDR_rmu","fake_lepvstopo_lowDR_rmu"]:
    hist = fake_rate.Get(histname)
    hist_ntup = root.TFile(histname+".root", "RECREATE")
    hist_ntup.cd()
    hist_ybins = hist.ProjectionY("bins",1,1)
    for i in range(hist.GetNbinsY()):
        ii = i+1
        hist_xbin = hist.ProjectionX(str(hist_ybins.GetBinLowEdge(ii))+"_"+str(hist_ybins.GetBinUpEdge(ii)),ii,ii)
        hist_xbin.Write()
    hist_ntup.Close()