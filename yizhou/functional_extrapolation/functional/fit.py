import ROOT as root
from array import array
import glob, os
from scipy import interpolate
import atlasplots as aplt
aplt.set_atlas_style()

mode = 1 # 0 for using all bins below 50, 1 for using first 2 bins
suffix = "_all" if mode==0 else "_2bins"
if not os.path.exists("plots_spline_all"): os.makedirs("plots_spline_all")
if not os.path.exists("plots_spline_2bins"): os.makedirs("plots_spline_2bins")

real_yrange = ["-8.0_1.0","1.0_3.0","3.0_6.0","6.0_10.0"]
fake_yrange = ["-8.0_1.0","1.0_3.0","3.0_6.0","6.0_10.0","10.0_30.0"]
rate_xbins = array('f',[27.0,30.0])
real_ybins = array('f',[-8.0,1.0,3.0,6.0,10.0])
fake_ybins = array('f',[-8.0,1.0,3.0,6.0,10.0,30.0])

hist_ntup = root.TFile("../hist_real_1btag_4jets_add.root", "RECREATE")
for file_name in glob.glob("real*.root"):
    short_name = file_name.split(".")[0]
    print(short_name)
    input_file = root.TFile.Open(file_name)
    hist_ntup.cd()
    hist2d = root.TH2D(short_name,short_name,1,rate_xbins,4,real_ybins)
    for hist_name in real_yrange:
        print(hist_name)
        ycen = (float(hist_name.split("_")[0])+float(hist_name.split("_")[1]))/2
        fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))
        hist = input_file.Get(hist_name)
        ax.plot(hist, "EP")
        xbins = hist.GetNbinsX()
        xi_no = 0
        ax.set_ylabel("Real_rate")
        ax.set_xlabel("Pt of lepton [GeV]")
        for i in range(xbins):
            if mode == 1 and xi_no >= 2: break
            if hist.GetBinContent(i+1) > 0 and hist.GetBinCenter(i+1) < 50.:
                xi_no += 1
        x = array('f', [0.0]*xi_no) 
        y = array('f', [0.0]*xi_no)
        ex = array('f', [0.0]*xi_no)
        ey = array('f', [0.0]*xi_no)
        xi_no0 = 0
        for i in range(xbins):
            if hist.GetBinContent(i+1) > 0 and hist.GetBinCenter(i+1) < 50. and xi_no0 < xi_no:
                x[xi_no0] = hist.GetBinCenter(i+1)
                y[xi_no0] = hist.GetBinContent(i+1)
                ex[xi_no0] = hist.GetBinWidth(i+1)/2
                ey[xi_no0] = hist.GetBinError(i+1)
                xi_no0 += 1
        def func_spline(in_x,par):
            tck = interpolate.splrep(x, y, k=xi_no0-1)
            return interpolate.splev(in_x[0], tck)
        fit_func = root.TF1("fit_func", func_spline,25.,100.)
        hist2d.Fill(28.5,ycen,func_spline([28.5],par=None))
        #fit_func = root.TF1("fit_func", "pol3",25.,50.)
        #fit_func = root.TF1("fit_func", "-exp([0]*x+[1])+1",25.,hist.GetXaxis().GetXmax())
        #if (xi_no0 >= 2): fit_func = root.TF1("fit_func", "[0]*x**2+[1]*x+[2]",25.,50.)
        #else: fit_func = root.TF1("fit_func", "[0]*x+[1]",25.,50.)
        #graph0.Fit("fit_func")
        ax.plot(fit_func, "L", linecolor=root.kRed+1, label="Fit", labelfmt="L")
        ax.add_margins(top=0.4, bottom=0.1)
        ax.legend(loc=(0.65, 0.67, 0.95, 0.77))
        aplt.atlas_label(text="Internal", loc="upper right")
        ax.text(0.6, 0.85, short_name, size=22, align=13)
        ax.text(0.6, 0.8, "topoetcone20 in "+hist_name, size=22, align=13)
        fig.savefig("plots_spline"+suffix+"/"+short_name+"_"+hist_name+".png")
    hist2d.Write()
hist_ntup.Close()

hist_ntup = root.TFile("../hist_fake_1btag_4jets_add.root", "RECREATE")
for file_name in glob.glob("fake*.root"):
    short_name = file_name.split(".")[0]
    print(short_name)
    input_file = root.TFile.Open(file_name)
    hist_ntup.cd()
    hist2d = root.TH2D(short_name,short_name,1,rate_xbins,5,fake_ybins)
    for hist_name in fake_yrange:
        print(hist_name)
        ycen = (float(hist_name.split("_")[0])+float(hist_name.split("_")[1]))/2
        fig, ax = aplt.subplots(1, 1, name="fig1", figsize=(800, 600))
        hist = input_file.Get(hist_name)
        ax.plot(hist, "EP")
        xbins = hist.GetNbinsX()
        xi_no = 0
        ax.set_ylabel("Real_rate")
        ax.set_xlabel("Pt of lepton [GeV]")
        for i in range(xbins):
            if mode == 1 and xi_no >= 2: break
            if hist.GetBinContent(i+1) > 0 and hist.GetBinCenter(i+1) < 50.:
                xi_no += 1
        x = array('f', [0.0]*xi_no) 
        y = array('f', [0.0]*xi_no)
        ex = array('f', [0.0]*xi_no)
        ey = array('f', [0.0]*xi_no)
        xi_no0 = 0
        for i in range(xbins):
            if hist.GetBinContent(i+1) > 0 and hist.GetBinCenter(i+1) < 50. and xi_no0 < xi_no:
                x[xi_no0] = hist.GetBinCenter(i+1)
                y[xi_no0] = hist.GetBinContent(i+1)
                ex[xi_no0] = hist.GetBinWidth(i+1)/2
                ey[xi_no0] = hist.GetBinError(i+1)
                xi_no0 += 1
        def func_spline(in_x,par):
            tck = interpolate.splrep(x, y, k=xi_no0-1)
            return interpolate.splev(in_x[0], tck)
        fit_func = root.TF1("fit_func", func_spline,25.,150.)
        hist2d.Fill(28.5,ycen,func_spline([28.5],par=None))
        #fit_func = root.TF1("fit_func", "pol3",25.,50.)
        #fit_func = root.TF1("fit_func", "-exp([0]*x+[1])+1",25.,hist.GetXaxis().GetXmax())
        #if (xi_no0 >= 2): fit_func = root.TF1("fit_func", "[0]*x**2+[1]*x+[2]",25.,50.)
        #else: fit_func = root.TF1("fit_func", "[0]*x+[1]",25.,50.)
        #graph0.Fit("fit_func")
        ax.plot(fit_func, "L", linecolor=root.kRed+1, label="Fit", labelfmt="L")
        ax.add_margins(top=0.4, bottom=0.1)
        ax.legend(loc=(0.65, 0.67, 0.95, 0.77))
        aplt.atlas_label(text="Internal", loc="upper right")
        ax.text(0.6, 0.85, short_name, size=22, align=13)
        ax.text(0.6, 0.8, "topoetcone20 in "+hist_name, size=22, align=13)
        fig.savefig("plots_spline"+suffix+"/"+short_name+"_"+hist_name+".png")
    hist2d.Write()
hist_ntup.Close()