import ROOT as root
from array import array
import glob, os
if not os.path.exists("hists"): os.makedirs("hists")

root.gInterpreter.AddIncludePath('/afs/desy.de/user/c/caiyi/dust/AHttbar/statana/TTbarNNLOReweighter/')
root.gROOT.LoadMacro('/afs/desy.de/user/c/caiyi/dust/AHttbar/statana/TTbarNNLOReweighter/macros/RecursiveReweighting_2d_2022.C')

minintuples_path = "/nfs/dust/atlas/user/caiyi/AHttbar/miniNtuples_new"
regions = ["4j1b","4j0b"]

fake_xbins_e = array('f',[27.0,30.0,35.0,40.0,60.0,80.0,100.0,500.0])
fake_xbins_mu = array('f',[27.0,30.0,35.0,40.0,50.0,70.0,100.0,500.0])
fake_ybins = array('f',[-8.0,1.0,3.0,6.0,10.0,30.0])

def get_chain(chain_,tree_,sample_,lep_,region_):
    suffix = "_MM" if tree_=="nominal_Loose" else ""
    file_name = "*"+(lep_+"_" if tree_=="nominal_Loose" else "")+lep_+"jets_"+region_+"_CR.root"
    if sample_=="data":
        for tfile in glob.glob(os.path.join(minintuples_path,sample_+suffix,file_name)):
            chain.Add(tfile)
    else:
        for tfile in glob.glob(os.path.join(minintuples_path,sample_+suffix,"*",file_name)):
            if not("multijet" in tfile): chain.Add(tfile)

for region in regions:
    for sample in ["mc","data"]:
        for tree in ["nominal","nominal_Loose"]:
            for lep in ["e","mu"]:
                print(sample+"_"+tree+"_"+lep)
                fake_xbins = fake_xbins_e if lep=="e" else fake_xbins_mu
                chain = root.TChain("nominal_Loose")
                get_chain(chain,"nominal_Loose",sample,lep,region)
                hist_ntup = root.TFile("hists/CR_"+sample+"_"+lep+"_"+tree+"_"+region+"_prompt.root", "RECREATE")
                hist_ntup.cd()
                hist2d_highDR = root.TH2D(lep+"_highDR",lep+"_highDR",len(fake_xbins)-1,fake_xbins,len(fake_ybins)-1,fake_ybins)
                hist2d_highDR.GetXaxis().SetTitle("pT_l [GeV]")
                hist2d_highDR.GetYaxis().SetTitle("topoetcone20 [GeV]")
                hist2d_lowDR = root.TH2D(lep+"_lowDR",lep+"_lowDR",len(fake_xbins)-1,fake_xbins,len(fake_ybins)-1,fake_ybins)
                hist2d_lowDR.GetXaxis().SetTitle("pT_l [GeV]")
                hist2d_lowDR.GetYaxis().SetTitle("topoetcone20 [GeV]")
                for i in range(chain.GetEntries()):
                    chain.GetEntry(i)
                    if sample=="mc":
                        prompt = chain.el_true_isPrompt[0] if lep=="e" else chain.mu_true_isPrompt[0]
                        if ord(prompt)==0: continue
                    isTight = ord(chain.el_isTight[0]) if lep=="e" else ord(chain.mu_isTight[0])
                    if tree=="nominal" and isTight==0: continue
                    pt_l = chain.el_pt[0] if lep=="e" else chain.mu_pt[0]
                    pt_l = min(pt_l/1000.,300.)
                    topoet = chain.el_topoetcone20[0] if lep=="e" else chain.mu_topoetcone20[0]
                    topoet = min(topoet/1000.,30.)
                    topoet = max(topoet,-8.)
                    weight = chain.weight_mc*chain.w_totalMinusMC*139000 if sample=="mc" else 1.
                    try:
                        if chain.MC_t_afterFSR_pt>0: weight = weight*root.RecursiveReweighting_2d_2022(chain.MC_t_afterFSR_pt/1e3,chain.MC_tbar_afterFSR_pt/1e3,chain.MC_ttbar_afterFSR_m/1e3,0)
                    except:
                        aaa = 1
                    if chain.minLeptonJetDeltaR>=0.4:
                        hist2d_highDR.Fill(pt_l,topoet,weight)
                    else:
                        hist2d_lowDR.Fill(pt_l,topoet,weight)
                hist_ntup.cd()
                hist2d_highDR.Write()
                hist2d_lowDR.Write()
                hist_ntup.Close()