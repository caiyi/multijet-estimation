import ROOT as root
from array import array

regions = ["4j1b","4j0b"]
for region in regions:
    new_ntup = root.TFile("../hist_fake_1btag_4jets_"+region+"_rw.root", "RECREATE")
    for lep in ["e","mu"]:
        ntup_data = root.TFile.Open("hists/CR_data_"+lep+"_nominal_"+region+"_rw.root","READ")
        ntup_data_Loose = root.TFile.Open("hists/CR_data_"+lep+"_nominal_Loose_"+region+"_rw.root","READ")
        ntup_mc = root.TFile.Open("hists/CR_mc_"+lep+"_nominal_"+region+"_rw.root","READ")
        ntup_mc_Loose = root.TFile.Open("hists/CR_mc_"+lep+"_nominal_Loose_"+region+"_rw.root","READ")
        new_ntup.cd()
        for hist_key in ntup_data.GetListOfKeys():
            hist_name = hist_key.GetTitle()
            new_hist_name = "fake_lepvstopo_"+hist_name.split("_")[-1]+"_r"+lep
            hist_data = ntup_data.Get(hist_name)
            hist_data_Loose = ntup_data_Loose.Get(hist_name)
            hist_mc = ntup_mc.Get(hist_name)
            hist_mc_Loose = ntup_mc_Loose.Get(hist_name)
            hist_tight = hist_data.Clone(new_hist_name)
            hist_tight.Add(hist_mc,-1)
            hist_loose = hist_data_Loose.Clone("tmp")
            hist_loose.Add(hist_mc_Loose,-1)
            hist_tight.Divide(hist_loose)
            hist_tight.Write()
    new_ntup.Close()