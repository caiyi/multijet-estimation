import ROOT as root
import glob
root.gStyle.SetOptStat(0)

for fname in glob.glob("./*real*.root"):
    real_rate = root.TFile.Open(fname)
    for histname in ["real_lepvstopo_highDR_re","real_lepvstopo_lowDR_re","real_lepvstopo_highDR_rmu","real_lepvstopo_lowDR_rmu"]:
        c = root.TCanvas("canvas", "canvas", 800 , 600)
        hist = real_rate.Get(histname)
        hist.GetZaxis().SetRangeUser(0, 1)
        hist.SetTitle(histname.split("_r")[-1]+"_"+histname.split("_")[2])
        hist.Draw("COLZ")
        c.SaveAs("real_"+histname.split("_r")[-1]+"_"+histname.split("_")[2]+"_"+fname.split(".")[1].split("_4jets_")[-1]+".png")
        
for fname in glob.glob("./*fake*.root"):
    fake_rate = root.TFile.Open(fname)
    for histname in ["fake_lepvstopo_highDR_re","fake_lepvstopo_lowDR_re","fake_lepvstopo_highDR_rmu","fake_lepvstopo_lowDR_rmu"]:
        c = root.TCanvas("canvas", "canvas", 800 , 600)
        hist = fake_rate.Get(histname)
        hist.GetZaxis().SetRangeUser(0, 1)
        hist.SetTitle(histname.split("_r")[-1]+"_"+histname.split("_")[2])
        hist.Draw("COLZ")
        c.SaveAs("fake_"+histname.split("_r")[-1]+"_"+histname.split("_")[2]+"_"+fname.split(".")[1].split("_4jets_")[-1]+".png")