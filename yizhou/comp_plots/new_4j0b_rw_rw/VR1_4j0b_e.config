% --------------- %
% ---  JOB    --- %
% --------------- %

Job:			"VR1_4j0b_e"
  CmeLabel: 		"13 TeV"
  ReadFrom: 		NTUP
  HistoPath: 		"/nfs/dust/atlas/user/caiyi/AHttbar/mm_esti/yizhou/vali_hists/hists/"
  Label: 		"A/H #rightarrow t#bar{t}, 1L"
  LumiLabel: 		"139 fb^{-1}"
  Lumi: 		139000
  Selection: 		"1"
  POI: "sqrt_mu"
  ReadFrom: HIST
  PlotOptions: 		"NOSIG"
  SummaryLogY: 		FALSE
  SummaryCanvasSize: 	800,800
  %SummaryPlotYmax: 	10e9
  NtupleName: 		"nominal"
  DebugLevel: 		1
  %StatOnly: 		TRUE
  MCstatThreshold: 	0.001
  SystControlPlots: 	TRUE
  SystErrorBars: TRUE
  SystDataPlots: 	TRUE
  % PruningType: 		COMBINEDSIGNAL
  %SystPruningShape: 	0.001
  %SystPruningNorm: 	0.001
  %SystLarge: 		0.90
  CorrelationThreshold: 0.20
  HistoChecks: 		NOCRASH
  SplitHistoFiles: 	TRUE 
  ImageFormat: 		"png"
  %SystCategoryTables: 	TRUE
  LegendNColumns: 	1
  LegendX1: 		0.6
  SmoothingOption:	TTBARRESONANCE 
  RatioYmax: 1.25
  RatioYmin: 0.75

  

% --------------- %
% ---  LIMIT  --- %
% --------------- %

Limit:			"limit"
  LimitType: 		ASYMPTOTIC
  LimitBlind: 		TRUE
  %SignalInjection: 	TRUE
  ConfidenceLevel: 	0.95

NormFactor:		"sqrt_mu"
  Samples: 		none
  Nominal: 		0
  Min: 			-10
  Max: 			10

% --------------- %
% --- REGIONS --- %
% --------------- %

Region:			         "met"
  Type: 		         VALIDATION
  VariableTitle: 	   "MET [GeV]"
  HistoName: 		     "MET"
  Label: 		         "e VR1 4j0b"

Region:			         "mwt"
  Type: 		         VALIDATION
  VariableTitle: 	   "MWT [GeV]"
  HistoName: 		     "MWT"
  Label: 		         "e VR1 4j0b"

Region:			         "pt_l"
  Type: 		         VALIDATION
  VariableTitle: 	   "pT(lepton) [GeV]"
  LogScale:          True
  HistoName: 		     "ptl"
  Label: 		         "e VR1 4j0b"

Region:			         "pt_j1"
  Type: 		         VALIDATION
  VariableTitle: 	   "pT(leading-jet)
  LogScale:          True
  HistoName: 		     "ptj1"
  Label: 		         "e VR1 4j0b"

Region:			         "pt_j2"
  Type: 		         VALIDATION
  VariableTitle: 	   "pT(subleading-jet)
  LogScale:          True
  HistoName: 		     "ptj2"
  Label: 		         "e VR1 4j0b"

Region:			         "m_ttbar"
  Type: 		         VALIDATION
  VariableTitle: 	   "M(ttbar) [GeV]"
  HistoName: 		     "Mttbar"
  Label: 		         "e VR1 4j0b"
  LogScale:          True

Region:			         "cosTheta"
  Type: 		         VALIDATION
  VariableTitle: 	   "cos(#theta*)"
  HistoName: 		     "cosThetaStar"
  Label: 		         "e VR1 4j0b"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: 		"Data"
   Title: 		"Data"
   Type: 		DATA
   HistoFile:		"data_e_4j0b_VR1_rw"

Sample: 		"ttbar"
  Type: 		BACKGROUND
  Title: 		"t#bar{t}"
  FillColor: 		600
  LineColor: 		1
  HistoFile: 	"mc_ttbar_e_4j0b_VR1_rw"

Sample:			"wjets2211"
  Type: 		BACKGROUND
  Title: 		"W+jets"
  FillColor: 		800
  LineColor: 		1
  HistoFile: 	"mc_wjets2211_e_4j0b_VR1_rw"

Sample:			"multijet"
  Type: 		BACKGROUND
  Title: 		"Multijet"
  NormalizedByTheory:   False
  FillColor: 		0
  LineColor: 		1
  HistoFile: 	"../hists_MM/multijet_e_4j0b_VR1_rw"

Sample:			"zjets2211"
  Type: 		BACKGROUND
  Title: 		"Z+jets"
  FillColor: 		920
  LineColor: 		1
  HistoFile: 	"mc_zjets2211_e_4j0b_VR1_rw"

Sample:			"vv"
  Type: 		BACKGROUND
  Title: 		"VV"
  FillColor: 		416
  LineColor: 		1
  HistoFile: 	"mc_vv_e_4j0b_VR1_rw"

Sample:			"ttbarv"
  Type: 		BACKGROUND
  Title: 		"t#bar{t}+V"
  FillColor: 		400
  LineColor: 		1
  HistoFile: 	"mc_ttbarv_e_4j0b_VR1_rw"

Sample:			"singletop"
  Type: 		BACKGROUND
  Title: 		"Single top"
  FillColor: 		616
  LineColor: 		1
  HistoFile: 	"mc_singletop_e_4j0b_VR1_rw"

% ---------------- %
% -- SYSTEMATIC -- %
% ---------------- %

Systematic: "QCD"
  Title: "Multijet Syst"
  Type: OVERALL
  OverallUp: 0.5
  OverallDown: -0.5
  Samples: multijet