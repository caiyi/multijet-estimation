import ROOT as root
import glob, os

test_no = 0
test_no = str(test_no) if test_no else ""
if not os.path.exists("hists_MM_new"): os.makedirs("hists_MM_new")
minintuples_path = "/nfs/dust/atlas/user/caiyi/AHttbar/miniNtuples_2"
#bregions = ["4j1b","4j0b","3j1b","2j1b"]
bregions = ["2j1b"]

hist_dict = {"MET":{'title':"MET [GeV]",'value':"met_met",'nbins':60,'xmin':0,'xmax':300,'/1e3':True},
             "MWT":{'title':"MWT [GeV]",'value':"mwt",'nbins':40,'xmin':0,'xmax':200,'/1e3':True},
             "Mttbar":{'title':"M(ttbar) [GeV]",'value':"chi2_ttbar_m",'nbins':34,'xmin':300,'xmax':2000,'/1e3':True},
             "Mttbar_B":{'title':"M(ttbar) [GeV]",'value':"vrcjet_3_5m_t_boosted_mtt",'nbins':27,'xmin':300,'xmax':3000,'/1e3':True},
             "cosThetaStar":{'title':"cos(#theta*)",'value':"chi2_cosThetaStar",'nbins':40,'xmin':-1,'xmax':1,'/1e3':False},
             "ptj1":{'title':"pT(leading-jet)",'value':"jet_pt[0]",'nbins':40,'xmin':0,'xmax':400,'/1e3':True},
             "ptj2":{'title':"pT(subleading-jet)",'value':"jet_pt[1]",'nbins':30,'xmin':0,'xmax':300,'/1e3':True},
             "ptl":{'title':"pT(lepton)",'value':"lep_pt[0]",'nbins':40,'xmin':0,'xmax':200,'/1e3':True}}

def get_chain(chain_,region_,sample_name_,lep_):
    file_name = "*"+lep_+"jets_"+bregion+"_"+region+".root"
    for tfile in glob.glob(os.path.join(minintuples_path,sample_name_,file_name)):
        print(tfile)
        chain.Add(tfile)

def get_rate(pt__,topoet__,name__):
    hist = ntup_dict[name__]
    xbin = hist.GetXaxis().FindBin(pt__)
    ybin = hist.GetYaxis().FindBin(topoet__)
    value = hist.GetBinContent(xbin,ybin)
    return value

def get_weight(pt_,topoet_,isTight_,minDR_,lep_):
    pt_ = min(pt_,300.)
    topoet_ = max(topoet_,-7.999)
    if minDR_>=0.4: dr = "high"
    else: dr ="low"
    fake_rate = get_rate(pt_,min(topoet_,29.999),"f_"+dr+"_"+lep_)
    real_rate = get_rate(pt_,min(topoet_,9.999),"r_"+dr+"_"+lep_)
    return fake_rate*(real_rate-isTight_)/(real_rate - fake_rate) if (real_rate - fake_rate)!=0 else 0.0

for bregion in bregions:
    fake_ntup = root.TFile.Open("../hist_fake_1btag_4jets_"+bregion+".root","READ")
    real_ntup = root.TFile.Open("../hist_real_1btag_4jets_"+bregion+".root","READ")
    fake_highDR_e = fake_ntup.Get("fake_lepvstopo_highDR_re")
    fake_lowDR_e = fake_ntup.Get("fake_lepvstopo_lowDR_re")
    fake_highDR_mu = fake_ntup.Get("fake_lepvstopo_highDR_rmu")
    fake_lowDR_mu = fake_ntup.Get("fake_lepvstopo_lowDR_rmu")
    real_highDR_e = real_ntup.Get("real_lepvstopo_highDR_re")
    real_lowDR_e = real_ntup.Get("real_lepvstopo_lowDR_re")
    real_highDR_mu = real_ntup.Get("real_lepvstopo_highDR_rmu")
    real_lowDR_mu = real_ntup.Get("real_lepvstopo_lowDR_rmu")
    ntup_dict = {"f_high_e":fake_highDR_e,"f_low_e":fake_lowDR_e,"f_high_mu":fake_highDR_mu,"f_low_mu":fake_lowDR_mu,"r_high_e":real_highDR_e,"r_low_e":real_lowDR_e,"r_high_mu":real_highDR_mu,"r_low_mu":real_lowDR_mu}
    for region in ["CR","VR1","VR2","SR"]:
        for sample in ["data_MM"]:
            for lep in ["e","mu"]:
                print(sample+"_"+lep+"_"+region)
                chain = root.TChain("nominal_Loose")
                get_chain(chain,region,sample,lep)
                hist_ntup = root.TFile("hists_MM_new/multijet"+test_no+"_"+lep+"_"+bregion+"_"+region+".root", "RECREATE")
                hist_ntup.cd()
                for h in hist_dict.keys():
                    hist_dict[h]['hist'] = root.TH1D(h,hist_dict[h]['title'],hist_dict[h]['nbins'],hist_dict[h]['xmin'],hist_dict[h]['xmax'])
                for i in range(chain.GetEntries()):
                    chain.GetEntry(i)
                    if lep=="e": weight=get_weight(chain.el_pt[0]/1000.,chain.el_topoetcone20[0]/1000.,ord(chain.el_isTight[0]),chain.minLeptonJetDeltaR,lep)
                    else: weight=get_weight(chain.mu_pt[0]/1000.,chain.mu_topoetcone20[0]/1000.,ord(chain.mu_isTight[0]),chain.minLeptonJetDeltaR,lep)
                    for h in hist_dict.keys():
                            if h=="ptl":
                                value = (chain.el_pt[0] if lep=="e" else chain.mu_pt[0])*1e-3
                            elif h=="ptj1":
                                value = chain.jet_pt[0]*1e-3
                            elif h=="ptj2":
                                value = chain.jet_pt[1]*1e-3
                            else: 
                                value = chain.GetLeaf(hist_dict[h]['value']).GetValue()*(1e-3 if hist_dict[h]['/1e3'] else 1.)
                            hist_dict[h]['hist'].Fill(value,weight)
                for h in hist_dict.keys():
                    hist_dict[h]['hist'].Write()
                hist_ntup.Close()