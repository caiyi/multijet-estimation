import ROOT as root
#import atlasplots as aplt
#aplt.set_atlas_style()
import glob, os
from array import array

if not os.path.exists("hists_tmp"): os.makedirs("hists_tmp")
minintuples_path = "/nfs/dust/atlas/user/caiyi/AHttbar/miniNtuples_2"
#bregions = ["4j1b","4j0b"]
bregions = ["4j1b"]
#regions = ["CR","VR1","VR2","SR"]
regions = ["CR"]

xbins_e = array('f',[27.0,30.0,35.0,40.0,60.0,80.0,100.0,500.0])
xbins_mu = array('f',[27.0,30.0,35.0,40.0,50.0,70.0,100.0,500.0])
ybins = array('f',[-8.0,1.0,3.0,6.0,10.0,30.0])

hist_dict = {"MET":{'title':"MET [GeV]",'value':"met_met",'nbins':60,'xmin':0,'xmax':300,'/1e3':True},
             "MWT":{'title':"MWT [GeV]",'value':"mwt",'nbins':40,'xmin':0,'xmax':200,'/1e3':True},
             "Mttbar":{'title':"M(ttbar) [GeV]",'value':"chi2_ttbar_m",'nbins':34,'xmin':300,'xmax':2000,'/1e3':True},
             "Mttbar_B":{'title':"M(ttbar) [GeV]",'value':"vrcjet_3_5m_t_boosted_mtt",'nbins':27,'xmin':300,'xmax':3000,'/1e3':True},
             "cosThetaStar":{'title':"cos(#theta*)",'value':"chi2_cosThetaStar",'nbins':40,'xmin':-1,'xmax':1,'/1e3':False},
             "ptj1":{'title':"pT(leading-jet)",'value':"jet_pt[0]",'nbins':40,'xmin':0,'xmax':400,'/1e3':True},
             "ptj2":{'title':"pT(subleading-jet)",'value':"jet_pt[1]",'nbins':30,'xmin':0,'xmax':300,'/1e3':True},
             "ptl":{'title':"pT(lepton)",'value':"lep_pt[0]",'nbins':40,'xmin':0,'xmax':200,'/1e3':True}}

def get_chain(chain_,region_,sample_name_,lep_):
    file_name = "*"+lep_+"jets_"+bregion+"_"+region+".root"
    for tfile in glob.glob(os.path.join(minintuples_path,sample_name_,file_name)):
        print(tfile)
        chain.Add(tfile)

for bregion in bregions:
    for region in regions:
        for sample in ["data","mc"]:
            #for lep in ["e","mu"]:
            for lep in ["e"]:
                print(sample+"_"+lep+"_"+region)
                if sample=="data":
                    samples = ["data"]
                else:
                    samples = ["mc/"+f for f in os.listdir(os.path.join(minintuples_path,sample))]
                for sample_name in samples:
                    if "multijet" in sample_name: chain = root.TChain("nominal_Loose")
                    else: chain = root.TChain("nominal")
                    get_chain(chain,region,sample_name,lep)
                    hist_ntup = root.TFile("hists_tmp/"+sample_name.replace("/","_")+"_"+lep+"_"+bregion+"_"+region+".root", "RECREATE")
                    hist_ntup.cd()
                    for h in hist_dict.keys():
                        hist_dict[h]['hist'] = root.TH1D(h,hist_dict[h]['title'],hist_dict[h]['nbins'],hist_dict[h]['xmin'],hist_dict[h]['xmax'])
                    for i in range(chain.GetEntries()):
                        chain.GetEntry(i)
                        pt = chain.el_pt[0]/1e3 if lep=="e" else chain.mu_pt[0]/1e3
                        topoet = chain.el_topoetcone20[0]/1e3 if lep=="e" else chain.mu_topoetcone20[0]/1e3
                        if pt<35: continue
                        if pt>40: continue
                        if topoet<10: continue
                        if chain.minLeptonJetDeltaR<0.4: continue
                        if sample=="data": weight = 1.
                        elif "multijet" in sample_name: weight = chain.multijetWeight
                        else: weight = chain.weight_mc*chain.w_totalMinusMC
                        for h in hist_dict.keys():
                            if h=="ptl":
                                value = (chain.el_pt[0] if lep=="e" else chain.mu_pt[0])*1e-3
                            elif h=="ptj1":
                                value = chain.jet_pt[0]*1e-3
                            elif h=="ptj2":
                                value = chain.jet_pt[1]*1e-3
                            else: 
                                value = chain.GetLeaf(hist_dict[h]['value']).GetValue()*(1e-3 if hist_dict[h]['/1e3'] else 1.)
                            hist_dict[h]['hist'].Fill(value,weight)
                    for h in hist_dict.keys():
                        hist_dict[h]['hist'].Write()
                    hist_ntup.Close()