import ROOT as root
#import atlasplots as aplt
#aplt.set_atlas_style()
import glob, os
root.gInterpreter.AddIncludePath('/afs/desy.de/user/c/caiyi/dust/AHttbar/statana/TTbarNNLOReweighter/')
root.gROOT.LoadMacro('/afs/desy.de/user/c/caiyi/dust/AHttbar/statana/TTbarNNLOReweighter/macros/RecursiveReweighting_2d_2022.C')

if not os.path.exists("hists"): os.makedirs("hists")
minintuples_path = "/nfs/dust/atlas/user/caiyi/AHttbar/miniNtuples_new"
#bregions = ["4j1b","4j0b","3j1b","2j1b"]
bregions = ["4j0b"]

hist_dict = {"MET":{'title':"MET [GeV]",'value':"met_met",'nbins':60,'xmin':0,'xmax':300,'/1e3':True},
             "MWT":{'title':"MWT [GeV]",'value':"mwt",'nbins':40,'xmin':0,'xmax':200,'/1e3':True},
             "Mttbar":{'title':"M(ttbar) [GeV]",'value':"chi2_ttbar_m",'nbins':34,'xmin':300,'xmax':2000,'/1e3':True},
             "Mttbar_B":{'title':"M(ttbar) [GeV]",'value':"vrcjet_3_5m_t_boosted_mtt",'nbins':27,'xmin':300,'xmax':3000,'/1e3':True},
             "cosThetaStar":{'title':"cos(#theta*)",'value':"chi2_cosThetaStar",'nbins':40,'xmin':-1,'xmax':1,'/1e3':False},
             "ptj1":{'title':"pT(leading-jet)",'value':"jet_pt[0]",'nbins':40,'xmin':0,'xmax':400,'/1e3':True},
             "ptj2":{'title':"pT(subleading-jet)",'value':"jet_pt[1]",'nbins':30,'xmin':0,'xmax':300,'/1e3':True},
             "ptl":{'title':"pT(lepton)",'value':"lep_pt[0]",'nbins':40,'xmin':0,'xmax':200,'/1e3':True}}

def get_chain(chain_,region_,sample_name_,lep_):
    file_name = "*"+lep_+"jets_"+bregion+"_"+region+".root"
    for tfile in glob.glob(os.path.join(minintuples_path,sample_name_,file_name)):
        print(tfile)
        chain.Add(tfile)

for bregion in bregions:
    for region in ["CR","VR1","VR2","SR"]:
        for sample in ["data","mc"]:
            for lep in ["e","mu"]:
                print(sample+"_"+lep+"_"+region)
                if sample=="data":
                    samples = ["data"]
                else:
                    samples = ["mc/"+f for f in os.listdir(os.path.join(minintuples_path,sample))]
                for sample_name in samples:
                    if "multijet" in sample_name: chain = root.TChain("nominal_Loose")
                    else: chain = root.TChain("nominal")
                    get_chain(chain,region,sample_name,lep)
                    hist_ntup = root.TFile("hists/"+sample_name.replace("/","_")+"_"+lep+"_"+bregion+"_"+region+"_prompt.root", "RECREATE")
                    hist_ntup.cd()
                    for h in hist_dict.keys():
                        hist_dict[h]['hist'] = root.TH1D(h,hist_dict[h]['title'],hist_dict[h]['nbins'],hist_dict[h]['xmin'],hist_dict[h]['xmax'])
                    for i in range(chain.GetEntries()):
                        chain.GetEntry(i)
                        if sample=="mc":
                            prompt = chain.el_true_isPrompt[0] if lep=="e" else chain.mu_true_isPrompt[0]
                            if ord(prompt)==0: continue
                        if sample=="data": weight = 1.
                        elif "multijet" in sample_name: weight = chain.multijetWeight
                        else: weight = chain.weight_mc*chain.w_totalMinusMC
                        if sample_name=="mc/ttbar": weight = weight*root.RecursiveReweighting_2d_2022(chain.MC_t_afterFSR_pt/1e3,chain.MC_tbar_afterFSR_pt/1e3,chain.MC_ttbar_afterFSR_m/1e3,0)
                        for h in hist_dict.keys():
                            if h=="ptl":
                                value = (chain.el_pt[0] if lep=="e" else chain.mu_pt[0])*1e-3
                            elif h=="ptj1":
                                value = chain.jet_pt[0]*1e-3
                            elif h=="ptj2":
                                value = chain.jet_pt[1]*1e-3
                            else: 
                                value = chain.GetLeaf(hist_dict[h]['value']).GetValue()*(1e-3 if hist_dict[h]['/1e3'] else 1.)
                            hist_dict[h]['hist'].Fill(value,weight)
                    hist_ntup.cd()
                    for h in hist_dict.keys():
                        hist_dict[h]['hist'].Write()
                    hist_ntup.Close()
