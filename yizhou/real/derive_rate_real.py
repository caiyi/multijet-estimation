import ROOT as root
from array import array

regions = ["4j1b","4j0b"]
for region in regions:
    new_ntup = root.TFile("../hist_real_1btag_4jets_"+region+"_prompt.root", "RECREATE")
    for lep in ["e","mu"]:
        ntup_mc = root.TFile.Open("hists/SR_mc_"+lep+"_nominal_"+region+"_prompt.root","READ")
        ntup_mc_Loose = root.TFile.Open("hists/SR_mc_"+lep+"_nominal_Loose_"+region+"_prompt.root","READ")
        new_ntup.cd()
        for hist_key in ntup_mc.GetListOfKeys():
            hist_name = hist_key.GetTitle()
            new_hist_name = "real_lepvstopo_"+hist_name.split("_")[-1]+"_r"+lep
            hist_mc = ntup_mc.Get(hist_name)
            hist_mc_Loose = ntup_mc_Loose.Get(hist_name)
            hist = hist_mc.Clone(new_hist_name)
            hist.Divide(hist_mc_Loose)
            hist.Write()
    new_ntup.Close()