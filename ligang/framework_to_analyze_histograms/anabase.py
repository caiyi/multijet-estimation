from ROOT import *
import ROOT
import math
import os

gROOT.SetBatch(True)

def add_overflow(h1):
    if 'TH1' in h1.ClassName():
        nbins = h1.GetNbinsX()
        N0 = h1.GetBinContent(0)
        D0 = h1.GetBinError(0)
        N1 = h1.GetBinContent(1)
        D1 = h1.GetBinError(1)
        Nnbins = h1.GetBinContent(nbins)
        Dnbins = h1.GetBinError(nbins)
        Nnbins1 = h1.GetBinContent(nbins+1)
        Dnbins1 = h1.GetBinError(nbins+1)
        h1.SetBinContent(1, N1 + N0)
        h1.SetBinError(1,math.sqrt(D0*D0 + D1*D1))
        h1.SetBinContent(nbins, Nnbins + Nnbins+1)
        h1.SetBinError(nbins, math.sqrt(Dnbins*Dnbins + Dnbins1*Dnbins1))
    elif 'TH2' in h1.ClassName():
        nbinsx = h1.GetXaxis().GetNbins()
        nbinsy = h1.GetYaxis().GetNbins()

        for iy in [1, nbinsy]:
            iy0 = 0
            if iy == nbinsy:
                iy0 = nbinsy + 1
            for ix in range(nbinsx):
                n = h1.GetBinContent(ix+1, iy)
                d = h1.GetBinError(ix+1, iy)
                n0 = h1.GetBinContent(ix+1, iy0)
                d0 = h1.GetBinError(ix+1, iy0)
                h1.SetBinContent(ix+1, iy, n + n0)
                h1.SetBinError(ix+1, iy, math.sqrt(d*d + d0*d0))

        for ix in [1, nbinsx]:
            ix0 = 0
            if ix == nbinsx:
                ix0 = nbinsx + 1
            for iy in range(nbinsy):
                n = h1.GetBinContent(ix, iy+1)
                d = h1.GetBinError(ix, iy+1)
                n0 = h1.GetBinContent(ix0, iy+1)
                d0 = h1.GetBinError(ix0, iy+1)
                h1.SetBinContent(ix, iy+1, n + n0)
                h1.SetBinError(ix, iy+1, math.sqrt(d*d + d0*d0))






def h2d_to_h1d(h2):
    #print('h2 =', h2)
    nbinsx = h2.GetXaxis().GetNbins()
    nbinsy = h2.GetYaxis().GetNbins()
    nbins = nbinsx*nbinsy
    h1 = TH1F(h2.GetName()+'_to1d', '', nbins, 0, nbins)
    for i in range(nbinsx):
        for j in range(nbinsy):
            h1.SetBinContent(j*nbinsx + i+1, h2.GetBinContent(i+1,j+1))
            h1.SetBinError(j*nbinsx + i+1, h2.GetBinError(i+1,j+1))
            #h1.GetXaxis().SetBinLabel(j*nbinsx + i+1, 'X%iY%i' % (i, j))
    return h1
def h1d_to_h2d(h1, h2_ref):
    nbinsx = h2_ref.GetXaxis().GetNbins()
    nbinsy = h2_ref.GetYaxis().GetNbins()
    nbins = nbinsx*nbinsy
    h2 = h2_ref.Clone(h1.GetName()+'_to2d')
    for i in range(nbinsx):
        for j in range(nbinsy):
            h2.SetBinContent(i+1, j+1, h1.GetBinContent(j*nbinsx + i+1))
            h2.SetBinError(i+1, j+1, h1.GetBinError(j*nbinsx + i+1))
    return h2
class asample(object):
   def __init__(self, name='', filepath = '', attr='bkg', legname = '', drawopt=[], is_datadriven=False, region=None, flag_add_overflow=0):
      self.filepath = filepath
      self.histofile = None
      self.name = name
      self.legname = legname
      self.attr = attr
      self.region = ''
      self.sign = ''
      self.h1 = None
      self.h1_2dto1d = None
      self.var= 'tau_pt'
      self.sf = 1.
      self.is_datadriven = is_datadriven
      self.drawopt = drawopt
      self.region = region
      self.flag_add_overflow = flag_add_overflow
      if not self.is_datadriven:
         if not os.path.exists(filepath):
            print 'WARNING:', filepath, 'does NOT exist!'
         elif not self.is_datadriven:
            self.histofile = TFile(filepath, 'read')
            if self.histofile:
               print filepath, 'loaded!'
            else:
               print filepath, 'failed!'
      else:
         if self.region == None:
            print 'WARNING: no region is loaded!'
         elif 'aregion' not in str(type(region)):
            print 'WARNING:', self.region,'is NOT a region!'
         else:
            print self.region
   def set_flag_add_overflow(self, flag_add_overflow):
       self.flag_add_overflow = flag_add_overflow
   def get_hist_default(self, region, sign, var):
      if self.histofile == None:
         print 'WARNING: no histofile found!'
         return None
      if self.name == '':
         print 'WARNING: sample name is empty!'
         return None
      self.region = region
      self.sign = sign
      self.var = var
      histname = 'h_' + self.var + '_' + self.region+ '_' + self.sign + '_' + self.name
      self.h1 = self.histofile.Get(histname)
      if not self.h1:
         print 'WARNING:', self.name, 'histogram for', region, sign, var, 'is NOT getted!'
         return None
      if self.sf !=1:
         self.h1.Scale(self.sf)
      if self.flag_add_overflow:
         add_overflow(self.h1)
      if 'TH2' in self.h1.ClassName():
          self.h1_2dto1d = h2d_to_h1d(self.h1)
          #print('debug:', self.h1, self.h1_2dto1d)
      nopt = len(self.drawopt)
      if nopt>0:
         #print 'sample =', self.name, self.h1.Integral()
         self.h1.SetLineColor(self.drawopt[0])
         if self.h1_2dto1d != None:
             self.h1_2dto1d.SetLineColor(self.drawopt[0])
      if nopt>1:
         self.h1.SetFillColor(self.drawopt[1])
         if self.h1_2dto1d != None:
             self.h1_2dto1d.SetFillColor(self.drawopt[1])
      #return self.h1
   def get_hist_dd(self, var):
      self.h1 = self.region.get_hsub(var)
      if not self.h1:
         print 'WARNING: failed to get data-driven sample.'
         return None
      nopt = len(self.drawopt)
      if nopt>0:
         #print 'sample =', self.name, self.h1.Integral()
         self.h1.SetLineColor(self.drawopt[0])
      if nopt>1:
         self.h1.SetFillColor(self.drawopt[1])
      #return self.h1

   def get_hist(self, region, sign, var, is_2dto1d):
      #print 'is_datadriven =', self.is_datadriven
      if self.is_datadriven:
         self.get_hist_dd(var)
      else:
         self.get_hist_default(region, sign, var)
      if is_2dto1d==1 and 'TH2' in self.h1.ClassName():
          return self.h1_2dto1d
      else:
          return self.h1

class aregion():
   def __init__(self, region='SR', sign = 'OS', name=''):
      self.region = region
      self.sign = sign
      if name != '':
         self.name = name
      else:
         self.name = region + '_' + sign
      self.list_samples = []
      self.hdata = None # for data histograms
      #self.hbkgsum = None # sum of bkg histograms
      self.hbkg = [] # for bkg histograms
      self.hsig = [] # for signal histograms
      self.legnamebkg = [] # legend name for bkg histograms
      self.legnamesig = [] # legend name for sig histograms
      self.var = ''
      self.plotformats=['png', 'pdf']
      self.plotdir = ''
      self.hsub=None # subtracted histogram = data - bkgs
      self.showsig = 1
      self.is_2dto1d = 1
   def set_is_2dto1d(self, is_2dto1d):
       self.is_2dto1d = is_2dto1d
   def set_var(self, var):
      self.var = var
   def add_sample(self, sample):
      self.list_samples.append(sample)
   def add_samples(self, samples):
      self.list_samples = []
      for sam in samples:
         self.list_samples.append(sam)
   def load_hists(self, var, is_2dto1d=-1):
      self.set_var(var)
      self.hdata = None
      list_hdata = []
      self.hbkg = []
      self.hsig = []
      for s in self.list_samples:
         if is_2dto1d < 0 :
             h1 = s.get_hist(self.region, self.sign, self.var, self.is_2dto1d)
         else:
             h1 = s.get_hist(self.region, self.sign, self.var, is_2dto1d)
         #print('debug:', s, h1)
         if s.attr == 'Data' or s.attr == 'data':
            if self.hdata == None:
               self.hdata = h1
            list_hdata.append(h1)
         elif s.attr == 'bkg':
            self.hbkg.append(h1)
            self.legnamebkg.append(s.legname)
         elif s.attr == 'sig':
            self.hsig.append(h1)
            self.legnamesig.append(s.legname)
         else:
            print 'WARNING: sample', s.name,'of attribute', s.attr, 'is NOT recognized!'
      #self.hdata = list_hdata[0]
      if len(list_hdata) > 1:
         for i in range(1, len(list_hdata)):
            self.hdata.Add(list_hdata[i])
   def check_hists(self):
      flag = 0
      if self.hdata == None:
         print 'WARNING: data histogram is not found!'
      else:
         flag += 1
         print 'data histogram is loaded with NOE =', self.hdata.Integral()
      if len(self.hbkg) == 0:
         print 'WARNING: no any bkg histogram!'
      else:
         flag += 10
         for i in range(len(self.hbkg)):
            print self.legnamebkg[i], 'histogram is loaded with NOE =', self.hbkg[i].Integral()
      if len(self.hsig) == 0:
         print 'WARNING: no any sig histogram!'
      else:
         flag += 100
         for i in range(len(self.hsig)):
            print self.legnamesig[i], 'histogram is loaded with NOE =', self.hsig[i].Integral()
      if flag >= 1:
         return 1
      else:
         return 0
   def get_hsub(self,var, is_2dto1d):
      self.load_hists(var, is_2dto1d)
      flag = self.check_hists()
      if not flag:
         print 'WARNING: in get_hsub, histograms are NOT good!'
         return None
      hsubname = 'hsub_' + var + '_' + self.region + '_' + self.sign
      hsub = self.hdata.Clone(hsubname)
      for h in self.hbkg:
         hsub.Add(h, -1)
      print 'DEBUG: subtracted histogram NOE =', hsub.Integral()
      return hsub
   def plot(self,var, xtitle = '', plotname = '', plotdir = '', ytitle= '', ztitle = ''):
      self.load_hists(var)
      flag = self.check_hists()
      if not flag:
         print 'WARNING: in plot, histograms are NOT good!'
         return None
      nbins = self.hdata.GetNbinsX()
      xmin = self.hdata.GetXaxis().GetXmin()
      xmax = self.hdata.GetXaxis().GetXmax()
      #hbkgsum = TH1F('hbkgsum', '', nbins, xmin, xmax)
      #hbkgsum.Sumw2()
      hbkgsum = self.hdata.Clone('hbkgsum')
      for i in range(1, nbins+1):
         hbkgsum.SetBinContent(i,0)
         hbkgsum.SetBinError(i,0)
      hskbkg = THStack('hskbkg', '')
      #print 'hehe1', hbkgsum.GetName(), hbkgsum.Integral()
      for a in self.hbkg:
         #print a.GetName(), a.Integral(), a.GetNbinsX()
         hbkgsum.Add(a)
         hskbkg.Add(a)
      #print 'hehe2'
      #hrdata = TH1F('hrdata', '', nbins, xmin, xmax)
      hrdata = self.hdata.Clone('hrdata')
      hrunc = hbkgsum.Clone('hrunc')
      for i in range(1,nbins+1):
         n0 = hbkgsum.GetBinContent(i)
         if n0 == 0:
            hrdata.SetBinContent(i, 0)
            hrdata.SetBinError(i, 0)
            hrunc.SetBinContent(i, 1)
            hrunc.SetBinError(i, 0)
            continue
         dn0 = hbkgsum.GetBinError(i)
         n = self.hdata.GetBinContent(i)
         hrdata.SetBinContent(i, n/n0)
         hrdata.SetBinError(i, math.sqrt(n)/n0)
         hrunc.SetBinContent(i, 1)
         hrunc.SetBinError(i, dn0/n0)
      Cs = TCanvas('Cs', '', 600, 600)
      padsize = 0.4
      pad1 = TPad('pad1', '', 0.0, padsize, 1.0, 1.0)
      pad2 = TPad('pad2', '', 0.0, 0.0, 1.0, padsize)
      pad1.SetTopMargin(0.10)
      pad1.SetBottomMargin(0.0)
      pad2.SetTopMargin(0.01)
      pad2.SetBottomMargin(0.35)
      pad1.Draw()
      pad2.Draw()
      pad1.cd()
      self.hdata.Draw('PE')
      hskbkg.Draw('hist,same')
      hbkgsum.Draw('E2same')
      hbkgsum.SetFillColor(ROOT.kBlack)
      hbkgsum.SetFillStyle(3004)
      hbkgsum.SetMarkerSize(0)
      if self.showsig:
         for hs in self.hsig:
            hs.SetLineWidth(3)
            hs.Draw('hist,same')
      self.hdata.Draw('PEsame')
      gPad.RedrawAxis()
      xunit = ''
      if '[' in xtitle and ']' in xtitle:
         xunit = xtitle[xtitle.find('[')+1:xtitle.find(']')]
      ymax = self.hdata.GetMaximum()
      if hbkgsum.GetMaximum() > ymax:
         ymax = hbkgsum.GetMaximum()
      if ytitle != '':
          self.hdata.GetYaxis().SetTitle(ytitle)
      elif self.hdata.GetBinWidth(1) == self.hdata.GetBinWidth(2):
          self.hdata.GetYaxis().SetTitle('Events / %i %s' % ( self.hdata.GetBinWidth(1), xunit))
      else:
          self.hdata.GetYaxis().SetTitle('Events / %s' % (xunit))
      self.hdata.GetYaxis().SetTitleSize(0.05/(1-padsize))
      self.hdata.GetYaxis().SetTitleOffset(0.8)
      self.hdata.GetYaxis().SetLabelSize(0.05/(1-padsize))
      #self.hdata.GetYaxis().SetNdivisions(505)
      self.hdata.GetYaxis().SetRangeUser(0.01, ymax*1.2)
      leg1 = TLegend(0.65, 0.5, 0.95, 0.9)
      leg1.AddEntry(self.hdata, 'Data', 'PE')
      for i in range(len(self.hbkg)):
         j = len(self.hbkg) - i -1
         leg1.AddEntry(self.hbkg[j], self.legnamebkg[j], 'F')
      if self.showsig:
         for i in range(len(self.hsig)):
            leg1.AddEntry(self.hsig[i], self.legnamesig[i], 'L')
      leg1.AddEntry(hbkgsum, 'Unc.', 'F')
      leg1.SetFillStyle(0)
      leg1.SetBorderSize(0)
      leg1.Draw()
      pad2.cd()
      hrdata.Draw('PE')
      gPad.Update()
      #print 'xmin =', pad2.GetUxmin()
      #print 'xmax =', pad2.GetUxmax()
      line1 = TLine(pad2.GetUxmin(), 1, pad2.GetUxmax(), 1)
      line1.SetLineColor(ROOT.kBlack)
      line1.SetLineWidth(1)
      line1.Draw()
      hrunc.Draw('E2same')
      hrunc.SetFillColor(ROOT.kBlack)
      hrunc.SetFillStyle(3004)
      hrunc.SetMarkerSize(0)
      hrdata.Draw('PEsame')
      hrdata.GetXaxis().SetTitle(xtitle)
      hrdata.GetXaxis().SetTitleSize(0.04/padsize)
      hrdata.GetXaxis().SetTitleOffset(1.1)
      hrdata.GetXaxis().SetLabelSize(0.04/padsize)
      hrdata.GetYaxis().SetTitle('Data/Tot. Bkg')
      hrdata.GetYaxis().SetRangeUser(0.02, 1.98)
      #hrdata.GetYaxis().SetNdivisions(505)
      hrdata.GetYaxis().SetLabelSize(0.05/padsize)
      hrdata.GetYaxis().SetTitleSize(0.05/padsize)
      hrdata.GetYaxis().SetTitleOffset(0.5)
      fullplotname = 'Cs_' + var + '_' +self.region+'_'+self.sign
      if plotname != '':
         fullplotname += '_' + plotname
      if plotdir != '':
         fullplotname = plotdir + '/' + fullplotname
      for pf in self.plotformats:
         Cs.SaveAs(fullplotname+'.'+pf)

class histratio():
   def __init__(self, name = '', action = ''):
      self.list_regions = []
      self.action = action
      self.plotformats = ['png', 'pdf']
   def add_region(self, reg):
      self.list_regions.append(reg)
   def add_regions(self, regs):
      self.list_regions = []
      for reg in regs:
         self.list_regions.append(reg)

   def get_AoverB(self,var, filename='', xtitle = '', plotname = '', plotdir = '', ytitle='', ztitle = '', is_2dto1d=0):
      if len(self.list_regions) < 2:
         print 'WARNING: FF number of regions less than 2'
         return None
      regA = self.list_regions[0]
      regB = self.list_regions[1]
      hA = regA.get_hsub(var, is_2dto1d)
      hB = regB.get_hsub(var, is_2dto1d)
      nbinsx = -1
      nbinsy = -1
      flag_2d = 0
      if 'TH2' in hA.ClassName():
          flag_2d = 1
      if flag_2d == 1:
          hA0 = hA.Clone(hA.GetName() + '_backup')
          hB0 = hB.Clone(hB.GetName() + '_backup')
          nbinsx = hA.GetXaxis().GetNbins()
          nbinsy = hA.GetYaxis().GetNbins()
          hA = h2d_to_h1d(hA)
          hB = h2d_to_h1d(hB)
      nbins = hA.GetNbinsX()
      xmin = hA.GetXaxis().GetXmin()
      xmax = hA.GetXaxis().GetXmax()
      #hAoverB = TH1F('hAoverB', '', nbins, xmin, xmax)
      #hAoverB.SetBins(hA.GetBins)
      #hAoverB.Sumw2()
      hAoverB = hA.Clone('hAoverB')
      for i in range(1, nbins + 1):
         nA  = hA.GetBinContent(i)
         dnA = hA.GetBinError(i)
         nB  = hB.GetBinContent(i)
         dnB = hB.GetBinError(i)
         if nB == 0:
            hAoverB.SetBinContent(i, 0)
            hAoverB.SetBinError(i, 0)
            continue
         y = nA/nB
         dy = math.sqrt(pow(dnA/nB,2) + pow(nA*dnB/nB/nB,2))
         hAoverB.SetBinContent(i, y)
         hAoverB.SetBinError(i, dy)
      hAoverBcopy = hAoverB.Clone('hAoverBcopy')
      if filename == '':
         filename = 'Ratio_'+regA.name + '_' + regB.name
      xCs = 600
      if flag_2d == 1:
          xCs = 1500
      Cs = TCanvas('Cs', '', 600, 600)
      padsize = 0.4
      pad1 = TPad('pad1', '', 0.0, padsize, 1.0, 1.0)
      pad2 = TPad('pad2', '', 0.0, 0.0, 1.0, padsize)
      pad1.SetTopMargin(0.1)
      pad1.SetBottomMargin(0.0)
      pad2.SetTopMargin(0.01)
      pad2.SetBottomMargin(0.35)
      pad1.Draw()
      pad2.Draw()
      pad1.cd()
      hA.Draw('PE')
      hB.Draw('PEsame')
      hB.SetMarkerStyle(24)
      hB.SetMarkerColor(ROOT.kBlue)
      hB.SetLineColor(ROOT.kBlue)
      gPad.RedrawAxis()
      xunit = ''
      if '[' in xtitle and ']' in xtitle:
         xunit = xtitle[xtitle.find('[')+1:xtitle.find(']')]
      ymax = hA.GetMaximum()
      if hB.GetMaximum() > ymax:
         ymax = hB.GetMaximum()
      if hA.GetBinWidth(1) == hA.GetBinWidth(2) and flag_2d == 0:
         hA.GetYaxis().SetTitle('Events / %i %s' % ( hA.GetBinWidth(1), xunit))
      else:
         hA.GetYaxis().SetTitle('Events / %s' % ( xunit))
      hA.GetYaxis().SetTitleSize(0.05/(1-padsize))
      hA.GetYaxis().SetTitleOffset(0.8)
      hA.GetYaxis().SetLabelSize(0.05/(1-padsize))
      #hA.GetYaxis().SetNdivisions(505)
      hA.GetYaxis().SetRangeUser(0.01, ymax*2.)
      leg1 = TLegend(0.65, 0.7, 0.95, 0.9)
      leg1.AddEntry(hA, regA.name, 'PE')
      leg1.AddEntry(hB, regB.name, 'PE')
      leg1.SetFillStyle(0)
      leg1.SetBorderSize(0)
      leg1.Draw()
      pad2.cd()
      hAoverB.Draw('PE')
      gPad.Update()
      line1 = TLine(pad2.GetUxmin(), 1, pad2.GetUxmax(), 1)
      line1.SetLineColor(ROOT.kBlack)
      line1.SetLineWidth(1)
      line1.Draw()
      hAoverB.GetXaxis().SetTitle(xtitle)
      hAoverB.GetXaxis().SetTitleSize(0.05/padsize)
      hAoverB.GetXaxis().SetTitleOffset(1.1)
      hAoverB.GetXaxis().SetLabelSize(0.04/padsize)
      hAoverB.GetYaxis().SetTitle('Ratio')
      hAoverB.GetYaxis().SetRangeUser(0.0, 2.02)
      #hAoverB.GetYaxis().SetNdivisions(505)
      hAoverB.GetYaxis().SetLabelSize(0.04/padsize)
      hAoverB.GetYaxis().SetTitleSize(0.05/padsize)
      hAoverB.GetYaxis().SetTitleOffset(0.5)
      fullplotname = 'Cs_' + var + '_' + filename
      if plotname != '':
         fullplotname += '_' + plotname
      if plotdir != '':
         fullplotname = plotdir + '/' + fullplotname
      for pf in self.plotformats:
         Cs.SaveAs(fullplotname+'.'+pf)
      
      ratio_ymax = 0.
      for i in range(hAoverBcopy.GetNbinsX()):
          if hAoverBcopy.GetBinContent(i+1) > ratio_ymax:
              ratio_ymax = hAoverBcopy.GetBinContent(i+1)
          
      Cs_ratio = TCanvas('Cs_ratio', '', 600, 600)
      hAoverBcopy.Draw('PE')
      hAoverBcopy.GetXaxis().SetTitle(xtitle)
      hAoverBcopy.GetXaxis().SetTitleSize(0.05)
      hAoverBcopy.GetYaxis().SetTitleSize(0.05)
      hAoverBcopy.GetYaxis().SetLabelSize(0.04)
      hAoverBcopy.GetYaxis().SetRangeUser(0, ratio_ymax+0.2)
      hAoverBcopy.GetYaxis().SetTitle('Rate')
      fullplotname = 'Cs_ratio_' + var + '_' + filename
      if plotname != '':
         fullplotname += '_' + plotname
      if plotdir != '':
         fullplotname = plotdir + '/' + fullplotname
      for pf in self.plotformats:
         Cs_ratio.SaveAs(fullplotname+'.'+pf)

      fileAoverB = TFile(plotdir + '/' + filename + '.root', 'recreate')
      if flag_2d == 1:
          hAoverB_2d = h1d_to_h2d(hAoverBcopy, hA0)
          hAoverB_2d.Write(filename)
      else:
          hAoverBcopy.Write(filename)
      fileAoverB.Close()

   

