import math
import sys, getopt
import string
import os
from ROOT import *
#from pdgRounding import pdgRound
from anabase import * 
import ROOT

from array import array

gROOT.LoadMacro("AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()
ROOT.gStyle.SetPaintTextFormat("2.1f")
ROOT.gStyle.SetPalette(55)
gROOT.SetBatch(True)

actions = ['getff', 'applyff']
leptag = 'ejets_4j1b_CR'
leptag = 'mujets_4j1b_CR'


iact = 0
action = actions[iact]

if action == 'getff':
   root_dir = '../root_dir/v0'
elif action == 'applyff':
   root_dir = '../root_dir/v2'

flag_add_overflow=1
data = asample('data_MM_'+leptag, root_dir + '/hist_data_MM_'+leptag+'_NOMINAL.root', 'data', 'data', [ROOT.kBlack] , flag_add_overflow=flag_add_overflow)
ttbar = asample('ttbar_'+leptag, root_dir + '/hist_ttbar_'+leptag+'_NOMINAL.root', 'bkg', 't#bar{t}', [ROOT.kBlack, 2] , flag_add_overflow=flag_add_overflow)
ttbarv = asample('ttbarv_'+leptag, root_dir + '/hist_ttbarv_'+leptag+'_NOMINAL.root', 'bkg', 't#bar{t}V', [ROOT.kBlack, 3] , flag_add_overflow=flag_add_overflow)
singletop = asample('singletop_'+leptag, root_dir + '/hist_singletop_'+leptag+'_NOMINAL.root', 'bkg', 'single top', [ROOT.kBlack, 4] , flag_add_overflow=flag_add_overflow)
vv = asample('vv_'+leptag, root_dir + '/hist_vv_'+leptag+'_NOMINAL.root', 'bkg', 'VV', [ROOT.kBlack, 5] , flag_add_overflow=flag_add_overflow)
wjets2211 = asample('wjets2211_'+leptag, root_dir + '/hist_wjets2211_'+leptag+'_NOMINAL.root', 'bkg', 'W+jets', [ROOT.kBlack, 6] , flag_add_overflow=flag_add_overflow)
zjets2211 = asample('zjets2211_'+leptag, root_dir + '/hist_zjets2211_'+leptag+'_NOMINAL.root', 'bkg', 'Z+jets', [ROOT.kBlack, 7] , flag_add_overflow=flag_add_overflow)

samples = [ data, ttbar, ttbarv, singletop, vv, wjets2211, zjets2211 ]

Dttbar = asample('ttbar_'+leptag, root_dir + '/hist_ttbar_'+leptag+'_NOMINAL.root', 'data', 't#bar{t}', [ROOT.kBlack, 2] , flag_add_overflow=flag_add_overflow)
Dttbarv = asample('ttbarv_'+leptag, root_dir + '/hist_ttbarv_'+leptag+'_NOMINAL.root', 'data', 't#bar{t}V', [ROOT.kBlack, 3] , flag_add_overflow=flag_add_overflow)
Dsingletop = asample('singletop_'+leptag, root_dir + '/hist_singletop_'+leptag+'_NOMINAL.root', 'data', 'single top', [ROOT.kBlack, 4] , flag_add_overflow=flag_add_overflow)
Dvv = asample('vv_'+leptag, root_dir + '/hist_vv_'+leptag+'_NOMINAL.root', 'data', 'VV', [ROOT.kBlack, 5] , flag_add_overflow=flag_add_overflow)
Dwjets2211 = asample('wjets2211_'+leptag, root_dir + '/hist_wjets2211_'+leptag+'_NOMINAL.root', 'data', 'W+jets', [ROOT.kBlack, 6] , flag_add_overflow=flag_add_overflow)
Dzjets2211 = asample('zjets2211_'+leptag, root_dir + '/hist_zjets2211_'+leptag+'_NOMINAL.root', 'data', 'Z+jets', [ROOT.kBlack, 7] , flag_add_overflow=flag_add_overflow)
AllMCs = [ Dttbar, Dttbarv, Dsingletop, Dvv, Dwjets2211, Dzjets2211 ]


#for a in samples:
 #  if a.attr != 'data' and a.attr!= 'data':
  #    a.sf = 31.96/36.21


# fake factors
if action == 'getff' and 'ejets' in leptag:
   lepTight_lowDR = aregion('lepTight', 'lowDR')
   lepTight_lowDR.add_samples(samples)
   lepTight_lowDR.plot('el_pt0','p_{T} [GeV]','', 'pic/')
   lepTight_lowDR.plot('el_pt0_VS_el_topoet','','', 'pic/')
   #lepTight_lowDR.plot('el_pt0_VS_DRlj','','', 'pic/')
   lepLoose_lowDR = aregion('lepLoose', 'lowDR')
   lepLoose_lowDR.add_samples(samples)
   lepLoose_lowDR.plot('el_pt0','p_{T} [GeV]','', 'pic/')
   lepLoose_lowDR.plot('el_pt0_VS_el_topoet','','', 'pic/')
   #lepLoose_lowDR.plot('el_pt0_VS_DRlj','','', 'pic/')
   FakeRate_lowDR = histratio('FakeRate_lowDR')
   FakeRate_lowDR.add_regions([lepTight_lowDR, lepLoose_lowDR])
   FakeRate_lowDR.get_AoverB('el_pt0', 'fake_lepPt_lowDR_re', 'p_{T} [GeV]', '', 'pic/')
   FakeRate_lowDR.get_AoverB('el_pt0_VS_el_topoet', 'fake_lepvstopo_lowDR_re', 'p_{T} : Topoetcone20', '', 'pic/')
   #FakeRate_lowDR.get_AoverB('el_pt0_VS_DRlj', 'fake_lepvstopo_lowDR_re', 'p_{T} : #DeltaR', '', 'pic/')
   
   lepTight_highDR = aregion('lepTight', 'highDR')
   lepTight_highDR.add_samples(samples)
   lepTight_highDR.plot('el_pt0','p_{T} [GeV]','', 'pic/')
   lepTight_highDR.plot('el_pt0_VS_el_topoet','','', 'pic/')
   lepLoose_highDR = aregion('lepLoose', 'highDR')
   lepLoose_highDR.add_samples(samples)
   lepLoose_highDR.plot('el_pt0','p_{T} [GeV]','', 'pic/')
   lepLoose_highDR.plot('el_pt0_VS_el_topoet','','', 'pic/')
   FakeRate_highDR = histratio('FakeRate_highDR')
   FakeRate_highDR.add_regions([lepTight_highDR, lepLoose_highDR])
   FakeRate_highDR.get_AoverB('el_pt0', 'fake_lepPt_highDR_re', 'p_{T} [GeV]', '', 'pic/')
   FakeRate_highDR.get_AoverB('el_pt0_VS_el_topoet', 'fake_lepvstopo_highDR_re', 'p_{T} : Topoetcone20', '', 'pic/')

   AllMCs_lepTight_lowDR = aregion('lepTight', 'lowDR')
   AllMCs_lepTight_lowDR.add_samples(AllMCs)
   AllMCs_lepTight_lowDR.plot('el_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepTight_lowDR.plot('el_pt0_VS_el_topoet','','AllMCs', 'pic/')
   AllMCs_lepLoose_lowDR = aregion('lepLoose', 'lowDR')
   AllMCs_lepLoose_lowDR.add_samples(AllMCs)
   AllMCs_lepLoose_lowDR.plot('el_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepLoose_lowDR.plot('el_pt0_VS_el_topoet','','AllMCs', 'pic/')
   RealRate_lowDR = histratio('RealRate_lowDR')
   RealRate_lowDR.add_regions([AllMCs_lepTight_lowDR, AllMCs_lepLoose_lowDR])
   RealRate_lowDR.get_AoverB('el_pt0', 'real_lepPt_lowDR_re', 'p_{T} [GeV]', 'AllMCs', 'pic/')
   RealRate_lowDR.get_AoverB('el_pt0_VS_el_topoet', 'real_lepvstopo_lowDR_re', 'p_{T} : Topoetcone20', '', 'pic/')

   AllMCs_lepTight_highDR = aregion('lepTight', 'highDR')
   AllMCs_lepTight_highDR.add_samples(AllMCs)
   AllMCs_lepTight_highDR.plot('el_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepTight_highDR.plot('el_pt0_VS_el_topoet','','AllMCs', 'pic/')
   AllMCs_lepLoose_highDR = aregion('lepLoose', 'highDR')
   AllMCs_lepLoose_highDR.add_samples(AllMCs)
   AllMCs_lepLoose_highDR.plot('el_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepLoose_highDR.plot('el_pt0_VS_el_topoet','','AllMCs', 'pic/')
   RealRate_highDR = histratio('RealRate_highDR')
   RealRate_highDR.add_regions([AllMCs_lepTight_highDR, AllMCs_lepLoose_highDR])
   RealRate_highDR.get_AoverB('el_pt0', 'real_lepPt_highDR_re', 'p_{T} [GeV]', 'AllMCs', 'pic/')
   RealRate_highDR.get_AoverB('el_pt0_VS_el_topoet', 'real_lepvstopo_highDR_re', 'p_{T} : Topoetcone20', '', 'pic/')

if action == 'getff' and 'mujets' in leptag :
   lepTight_lowDR = aregion('lepTight', 'lowDR')
   lepTight_lowDR.add_samples(samples)
   lepTight_lowDR.plot('mu_pt0','p_{T} [GeV]','', 'pic/')
   lepTight_lowDR.plot('mu_pt0_VS_mu_topoet','','', 'pic/')
   lepLoose_lowDR = aregion('lepLoose', 'lowDR')
   lepLoose_lowDR.add_samples(samples)
   lepLoose_lowDR.plot('mu_pt0','p_{T} [GeV]','', 'pic/')
   lepLoose_lowDR.plot('mu_pt0_VS_mu_topoet','','', 'pic/')
   FakeRate_lowDR = histratio('FakeRate_lowDR')
   FakeRate_lowDR.add_regions([lepTight_lowDR, lepLoose_lowDR])
   FakeRate_lowDR.get_AoverB('mu_pt0', 'fake_lepPt_lowDR_rmu', 'p_{T} [GeV]', '', 'pic/')
   FakeRate_lowDR.get_AoverB('mu_pt0_VS_mu_topoet', 'fake_lepvstopo_lowDR_rmu', 'p_{T} : Topoetcone20', '', 'pic/')
   
   lepTight_highDR = aregion('lepTight', 'highDR')
   lepTight_highDR.add_samples(samples)
   lepTight_highDR.plot('mu_pt0','p_{T} [GeV]','', 'pic/')
   lepTight_highDR.plot('mu_pt0_VS_mu_topoet','','', 'pic/')
   lepLoose_highDR = aregion('lepLoose', 'highDR')
   lepLoose_highDR.add_samples(samples)
   lepLoose_highDR.plot('mu_pt0','p_{T} [GeV]','', 'pic/')
   lepLoose_highDR.plot('mu_pt0_VS_mu_topoet','','', 'pic/')
   FakeRate_highDR = histratio('FakeRate_highDR')
   FakeRate_highDR.add_regions([lepTight_highDR, lepLoose_highDR])
   FakeRate_highDR.get_AoverB('mu_pt0', 'fake_lepPt_highDR_rmu', 'p_{T} [GeV]', '', 'pic/')
   FakeRate_highDR.get_AoverB('mu_pt0_VS_mu_topoet', 'fake_lepvstopo_highDR_rmu', 'p_{T} : Topoetcone20', '', 'pic/')

   AllMCs_lepTight_lowDR = aregion('lepTight', 'lowDR')
   AllMCs_lepTight_lowDR.add_samples(AllMCs)
   AllMCs_lepTight_lowDR.plot('mu_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepTight_lowDR.plot('mu_pt0_VS_mu_topoet','','AllMCs', 'pic/')
   AllMCs_lepLoose_lowDR = aregion('lepLoose', 'lowDR')
   AllMCs_lepLoose_lowDR.add_samples(AllMCs)
   AllMCs_lepLoose_lowDR.plot('mu_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepLoose_lowDR.plot('mu_pt0_VS_mu_topoet','','AllMCs', 'pic/')
   RealRate_lowDR = histratio('RealRate_lowDR')
   RealRate_lowDR.add_regions([AllMCs_lepTight_lowDR, AllMCs_lepLoose_lowDR])
   RealRate_lowDR.get_AoverB('mu_pt0', 'real_lepPt_lowDR_rmu', 'p_{T} [GeV]', 'AllMCs', 'pic/')
   RealRate_lowDR.get_AoverB('mu_pt0_VS_mu_topoet', 'real_lepvstopo_lowDR_rmu', 'p_{T} : Topoetcone20', '', 'pic/')

   AllMCs_lepTight_highDR = aregion('lepTight', 'highDR')
   AllMCs_lepTight_highDR.add_samples(AllMCs)
   AllMCs_lepTight_highDR.plot('mu_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepTight_highDR.plot('mu_pt0_VS_mu_topoet','','AllMCs', 'pic/')
   AllMCs_lepLoose_highDR = aregion('lepLoose', 'highDR')
   AllMCs_lepLoose_highDR.add_samples(AllMCs)
   AllMCs_lepLoose_highDR.plot('mu_pt0','p_{T} [GeV]','AllMCs', 'pic/')
   AllMCs_lepLoose_highDR.plot('mu_pt0_VS_mu_topoet','','AllMCs', 'pic/')
   RealRate_highDR = histratio('RealRate_highDR')
   RealRate_highDR.add_regions([AllMCs_lepTight_highDR, AllMCs_lepLoose_highDR])
   RealRate_highDR.get_AoverB('mu_pt0', 'real_lepPt_highDR_rmu', 'p_{T} [GeV]', 'AllMCs', 'pic/')
   RealRate_highDR.get_AoverB('mu_pt0_VS_mu_topoet', 'real_lepvstopo_highDR_rmu', 'p_{T} : Topoetcone20', '', 'pic/')



