#ifndef myana_h
#define myana_h
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TChain.h"
#include "TString.h"
#include "TSystem.h"
#include <iostream>
#include "TLorentzVector.h"
//#include "ana.h"

class myana : public ana {
   public:
	TString SampleName;
	TString SysUncName;
	double N_AOD;
	int Nregions;
	int Nsigns;
	TString name_regions[100];
	TString name_signs[10];
	bool value_regions[100];
	double weight_regions[100];
	bool value_signs[10];
	bool Flag_draw_histograms;

	//declare regions
	//input_regions
	bool lepLoose;
	bool lepTight;

	//declare cuts
	//input_cuts
	bool is_data;

	//declare signs
	//input_signs
	bool allDR;
	bool lowDR;
	bool highDR;

	//declare variables whose histograms will be saved
	//declare extra variables which are not in the ntuples
	 double WeightBase;
	 double el_pt0;
	 double mu_pt0;
	 double el_topoet;
	 double mu_topoet;
	 double DRlj;
	 double met_met0;
	 double mwt0;

      //declare objects from external files

	//input_variables
	TH1F* h_el_pt0[2][3];
	TH1F* h_mu_pt0[2][3];
	TH1F* h_el_topoet[2][3];
	TH1F* h_mu_topoet[2][3];
	TH1F* h_DRlj[2][3];
	TH2F* h_el_pt0_VS_DRlj[2][3];
	TH2F* h_mu_pt0_VS_DRlj[2][3];
	TH2F* h_el_pt0_VS_el_topoet[2][3];
	TH2F* h_mu_pt0_VS_mu_topoet[2][3];
	TH1F* h_met_met0[2][3];
	TH1F* h_mwt0[2][3];



	myana(TChain* tree=0, TString samplename="", TString sysuncname="NOMINAL"): ana(tree) {
	   cout<<"class myana is constructed !"<<endl;
	   SampleName = samplename;
	   SysUncName = sysuncname;
	   Nregions = 2;
	   Nsigns = 3;
	   Flag_draw_histograms = 0;
	   //fill name_regions
	name_regions[0] = "lepLoose";
	name_regions[1] = "lepTight";

	   //fill name_signs
	name_signs[0] = "allDR";
	name_signs[1] = "lowDR";
	name_signs[2] = "highDR";


	}
	virtual ~myana(){};
	void SetSampleName(TString samplename=""){
	   SampleName = samplename;
	}
	void SetSysUncName(TString sysuncname=""){
	   SysUncName = sysuncname;
	}
	void SetFlag_draw_histograms(bool flag = false){
	   Flag_draw_histograms = flag;
	}
	//functions
	void InitHistograms();
	void RegisterCuts();
	void LoadExternalObjects();
	void Loop();
	void SaveHistograms();
	void Work(){
	   InitHistograms();
         LoadExternalObjects();
	   Loop();
	   SaveHistograms();
	}
};

#endif
