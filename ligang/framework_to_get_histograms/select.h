#ifndef select_h
#define select_h
#include "ana.C"
#include "myana.C"

void select(TString treename="NOMINAL", TString samplename="", TString samplepath1="", TString samplepath2=""){
  
   if(samplename == "" || samplepath1 == "") return;


   //gSystem -> Load("ana_C.so");
   //gSystem -> Load("myana_C.so");


   TChain* chain = new TChain(treename.Data());
   chain -> Add(samplepath1);
   if(samplepath2!="") chain -> Add(samplepath2);

   myana* analysis = new myana(chain, samplename);
   //fill flags
	analysis -> SetFlag_draw_histograms(1);

   analysis -> Work();

}



#endif 
