//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed May 18 09:52:07 2022 by ROOT version 6.20/06
// from TChain nominal_Loose/nominal_Loose
// found on file: wjets2211_mc16a_ejets_4j1b_CR.root
//////////////////////////////////////////////////////////

#ifndef ana_h
#define ana_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TChain if any.
#include "vector"
#include "vector"
#include "vector"

class ana {
public :
   TChain          *fChain;   //!pointer to the analyzed TChain or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TChain if any.

   // Declaration of leaf types
   Double_t        w_totalMinusMC;
   Float_t         weight_mc;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_pt;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_eta;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_phi;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_e;
   vector<float>   *el_m;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_charge;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *el_topoetcone20;
   vector<char,ROOT::Detail::VecOps::RAdoptAllocator<char> > *el_isTight;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_pt;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_eta;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_phi;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_e;
   vector<float>   *mu_m;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_charge;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *mu_topoetcone20;
   vector<char,ROOT::Detail::VecOps::RAdoptAllocator<char> > *mu_isTight;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *jet_pt;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *jet_eta;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *jet_phi;
   vector<float,ROOT::Detail::VecOps::RAdoptAllocator<float> > *jet_e;
   vector<float>   *jet_m;
   Float_t         chi2_wh_m;
   Float_t         chi2_wl_m;
   Double_t        w_lumi;
   Float_t         weight_norm;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_jvt;
   Float_t         weight_bTagSF_DL1r_77;
   Int_t           resolved_ejets;
   Int_t           resolved_mujets;
   Float_t         chi2_ttbar_m;
   Float_t         chi2_th_m;
   Float_t         chi2_tl_m;
   Float_t         log10chi2;
   Int_t           btag_cat_alt;
   Double_t        chi2_cosThetaStar;
   Double_t        cosThetaLeptonTtbar;
   Double_t        cosThetaLeptonLeastEnergeticJet;
   Double_t        cosThetaLeptonTop;
   Float_t         deltaPhiTopQuarks;
   Float_t         deltaEtaTopQuarks;
   Float_t         deltaPhiTopLepton;
   Float_t         deltaEtaTopLepton;
   Float_t         CosPhiLJ;
   Float_t         chi2_th_pt;
   Float_t         chi2_th_eta;
   Float_t         chi2_th_phi;
   Float_t         chi2_tl_pt;
   Float_t         chi2_tl_eta;
   Float_t         chi2_tl_phi;
   Double_t        chi2_ttbar_chi;
   Bool_t          isResolved;
   Bool_t          isBoosted;
   Int_t           n_jet;
   Int_t           n_bjet;
   Float_t         met_met;
   Float_t         mwt;
   Float_t         ptw;
   Float_t         minLeptonJetDeltaR;
   Float_t         vrcjet_3_5m_t_boosted_mtt;

   // List of branches
   TBranch        *b_w_totalMinusMC;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_isTight;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_isTight;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_chi2_wh_m;   //!
   TBranch        *b_chi2_wl_m;   //!
   TBranch        *b_w_lumi;   //!
   TBranch        *b_weight_norm;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_resolved_ejets;   //!
   TBranch        *b_resolved_mujets;   //!
   TBranch        *b_chi2_ttbar_m;   //!
   TBranch        *b_chi2_th_m;   //!
   TBranch        *b_chi2_tl_m;   //!
   TBranch        *b_log10chi2;   //!
   TBranch        *b_btag_cat_alt;   //!
   TBranch        *b_chi2_cosThetaStar;   //!
   TBranch        *b_cosThetaLeptonTtbar;   //!
   TBranch        *b_cosThetaLeptonLeastEnergeticJet;   //!
   TBranch        *b_cosThetaLeptonTop;   //!
   TBranch        *b_deltaPhiTopQuarks;   //!
   TBranch        *b_deltaEtaTopQuarks;   //!
   TBranch        *b_deltaPhiTopLepton;   //!
   TBranch        *b_deltaEtaTopLepton;   //!
   TBranch        *b_CosPhiLJ;   //!
   TBranch        *b_chi2_th_pt;   //!
   TBranch        *b_chi2_th_eta;   //!
   TBranch        *b_chi2_th_phi;   //!
   TBranch        *b_chi2_tl_pt;   //!
   TBranch        *b_chi2_tl_eta;   //!
   TBranch        *b_chi2_tl_phi;   //!
   TBranch        *b_chi2_ttbar_chi;   //!
   TBranch        *b_isResolved;   //!
   TBranch        *b_isBoosted;   //!
   TBranch        *b_n_jet;   //!
   TBranch        *b_n_bjet;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_mwt;   //!
   TBranch        *b_ptw;   //!
   TBranch        *b_minLeptonJetDeltaR;   //!
   TBranch        *b_vrcjet_3_5m_t_boosted_mtt;   //!

   ana(TChain *tree=0);
   virtual ~ana();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TChain *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ana_cxx
ana::ana(TChain *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("wjets2211_mc16a_ejets_4j1b_CR.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("wjets2211_mc16a_ejets_4j1b_CR.root");
      }
      f->GetObject("nominal_Loose",tree);

   }
   Init(tree);
}

ana::~ana()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ana::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ana::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ana::Init(TChain *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_m = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_isTight = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_m = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_isTight = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_m = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
//   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("w_totalMinusMC", &w_totalMinusMC, &b_w_totalMinusMC);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_isTight", &el_isTight, &b_el_isTight);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_isTight", &mu_isTight, &b_mu_isTight);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("chi2_wh_m", &chi2_wh_m, &b_chi2_wh_m);
   fChain->SetBranchAddress("chi2_wl_m", &chi2_wl_m, &b_chi2_wl_m);
   fChain->SetBranchAddress("w_lumi", &w_lumi, &b_w_lumi);
   fChain->SetBranchAddress("weight_norm", &weight_norm, &b_weight_norm);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
   fChain->SetBranchAddress("resolved_ejets", &resolved_ejets, &b_resolved_ejets);
   fChain->SetBranchAddress("resolved_mujets", &resolved_mujets, &b_resolved_mujets);
   fChain->SetBranchAddress("chi2_ttbar_m", &chi2_ttbar_m, &b_chi2_ttbar_m);
   fChain->SetBranchAddress("chi2_th_m", &chi2_th_m, &b_chi2_th_m);
   fChain->SetBranchAddress("chi2_tl_m", &chi2_tl_m, &b_chi2_tl_m);
   fChain->SetBranchAddress("log10chi2", &log10chi2, &b_log10chi2);
   fChain->SetBranchAddress("btag_cat_alt", &btag_cat_alt, &b_btag_cat_alt);
   fChain->SetBranchAddress("chi2_cosThetaStar", &chi2_cosThetaStar, &b_chi2_cosThetaStar);
   fChain->SetBranchAddress("cosThetaLeptonTtbar", &cosThetaLeptonTtbar, &b_cosThetaLeptonTtbar);
   fChain->SetBranchAddress("cosThetaLeptonLeastEnergeticJet", &cosThetaLeptonLeastEnergeticJet, &b_cosThetaLeptonLeastEnergeticJet);
   fChain->SetBranchAddress("cosThetaLeptonTop", &cosThetaLeptonTop, &b_cosThetaLeptonTop);
   fChain->SetBranchAddress("deltaPhiTopQuarks", &deltaPhiTopQuarks, &b_deltaPhiTopQuarks);
   fChain->SetBranchAddress("deltaEtaTopQuarks", &deltaEtaTopQuarks, &b_deltaEtaTopQuarks);
   fChain->SetBranchAddress("deltaPhiTopLepton", &deltaPhiTopLepton, &b_deltaPhiTopLepton);
   fChain->SetBranchAddress("deltaEtaTopLepton", &deltaEtaTopLepton, &b_deltaEtaTopLepton);
   fChain->SetBranchAddress("CosPhiLJ", &CosPhiLJ, &b_CosPhiLJ);
   fChain->SetBranchAddress("chi2_th_pt", &chi2_th_pt, &b_chi2_th_pt);
   fChain->SetBranchAddress("chi2_th_eta", &chi2_th_eta, &b_chi2_th_eta);
   fChain->SetBranchAddress("chi2_th_phi", &chi2_th_phi, &b_chi2_th_phi);
   fChain->SetBranchAddress("chi2_tl_pt", &chi2_tl_pt, &b_chi2_tl_pt);
   fChain->SetBranchAddress("chi2_tl_eta", &chi2_tl_eta, &b_chi2_tl_eta);
   fChain->SetBranchAddress("chi2_tl_phi", &chi2_tl_phi, &b_chi2_tl_phi);
   fChain->SetBranchAddress("chi2_ttbar_chi", &chi2_ttbar_chi, &b_chi2_ttbar_chi);
   fChain->SetBranchAddress("isResolved", &isResolved, &b_isResolved);
   fChain->SetBranchAddress("isBoosted", &isBoosted, &b_isBoosted);
   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("n_bjet", &n_bjet, &b_n_bjet);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("mwt", &mwt, &b_mwt);
   fChain->SetBranchAddress("ptw", &ptw, &b_ptw);
   fChain->SetBranchAddress("minLeptonJetDeltaR", &minLeptonJetDeltaR, &b_minLeptonJetDeltaR);
   fChain->SetBranchAddress("vrcjet_3_5m_t_boosted_mtt", &vrcjet_3_5m_t_boosted_mtt, &b_vrcjet_3_5m_t_boosted_mtt);
   Notify();
}

Bool_t ana::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TChain in a TChain or when when a new TChain
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ana::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ana::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ana_cxx
